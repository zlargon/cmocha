#include "mocha.h"

int test() {
    assert(1 == 1);
    assert(2 == 2);
    return 0;
}

int main() {
    describe(
        "Test Example",
        test
    );
    return 0;
}
